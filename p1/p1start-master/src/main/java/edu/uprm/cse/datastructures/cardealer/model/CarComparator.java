package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{
	private int value;
	
	@Override
	public int compare(Car c1, Car c2) {
		int brand, model, option;
		brand = ((c1.getCarBrand())).compareTo(c2.getCarBrand());
		model = (c1.getCarModel()).compareTo(c2.getCarModel());
		option = (c1.getCarModelOption()).compareTo(c2.getCarModelOption());
		if(brand!=0) {
			value=brand;
			return value;
		}
		if(model!=0) {
			value=model;
			return value;
		}
		value=option;
		return value;
	}

}
